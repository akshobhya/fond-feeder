#include "HX711.h"
#include <WiFi.h>
#include "ThingSpeak.h"
#include "HTTPClient.h"
#include "BLESense.h"
#include "Dispenser.h"



// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 23;
const int LOADCELL_SCK_PIN = 22;
const char* ssid = "https://akshobhya.me";
const char* password = "Shambhala";
WiFiClient  client;

unsigned long myChannelNumber = 1;
const char * myWriteAPIKey = "RHLN9OPTQ44BFRTC";

HX711 scale;
HTTPClient http;
void setup() {
  //Serial.begin(38400);
  Serial.begin(115200);
  BLESense_Setup();
  Dispenser_Setup();
  // Initialize library with data output pin, clock input pin and gain factor.
  // Channel selection is made by passing the appropriate gain:
  // - With a gain factor of 64 or 128, channel A is selected
  // - With a gain factor of 32, channel B is selected
  // By omitting the gain factor parameter, the library
  // default "128" (Channel A) is used here.

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  WiFi.mode(WIFI_STA);   
  
  ThingSpeak.begin(client);
  //WiFi.begin(ssid, password);
  // Timer variables

  
  if(WiFi.status() != WL_CONNECTED){
      Serial.print("Attempting to connect to the internet");
      while(WiFi.status() != WL_CONNECTED){
        WiFi.begin(ssid, password); 
        delay(3000);     
      } 
      Serial.println("\nConnected.");
    }
  
  //scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  Serial.println("Before setting up the scale:");
  Serial.print("read: \t\t");
  Serial.println(scale.read());      // print a raw reading from the ADC

  Serial.print("read average: \t\t");
  Serial.println(scale.read_average(20));   // print the average of 20 readings from the ADC

  Serial.print("get value: \t\t");
  Serial.println(scale.get_value(5));   // print the average of 5 readings from the ADC minus the tare weight (not set yet)

  Serial.print("get units: \t\t");
  Serial.println(scale.get_units(5), 1);  // print the average of 5 readings from the ADC minus tare weight (not set) divided
            // by the SCALE parameter (not set yet)

  scale.set_scale(701.f);
  scale.tare();              // reset the scale to 0
  
 
}

unsigned long lastTime = 0;
unsigned long timerDelay = 300;


void loop() {


  if ((millis() - lastTime) >= timerDelay) {
    
    // Connect or reconnect to WiFi
    if(WiFi.status() != WL_CONNECTED){
      Serial.print("Attempting to connect");
      while(WiFi.status() != WL_CONNECTED){
        WiFi.begin(ssid, password); 
        delay(5000);     
      } 
      Serial.println("\nConnected.");
    }
  }
  
  Serial.print("Reading:\t");
  float weight=scale.get_units();
  Serial.print(weight, 1);
  Serial.print("\t| average:\t");
  Serial.println(scale.get_units(10), 1);
  delay(5000);
  int x = ThingSpeak.writeField(myChannelNumber, 1, weight, myWriteAPIKey);
    if(x == 200){
      Serial.println("Channel update successful.");
    }
    else{
      Serial.println("Problem updating channel. HTTP error code " + String(x));
    }
    lastTime = millis();
  
  //scale.power_down();             // put the ADC in sleep mode
  
  //scale.power_up();
 
}
